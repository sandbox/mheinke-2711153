/**
 * Define plugins
 */
var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('gulp-autoprefixer'),
    lost = require('lost'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload');

/**
 * Define paths
 */
var paths = {
    scripts: 'js/**/*.js',
    css: 'css/scss/**/*.scss',
    dist: 'css'
}

/**
 * Compile SASS to CSS
 */
gulp.task('sass', function() {
    return gulp.src('css/scss/*.scss')
        .pipe(plumber())
        .pipe(sass.sync({
            outputStyle: 'compressed',
            errLogToConsole: true
        }))
        .pipe(autoprefixer({ browsers: ['last 2 versions'] }))
        .pipe(postcss([
            lost()
        ]))
        .pipe(gulp.dest('css'))
        .pipe(livereload())
});

/**
 * Watch for changes
 */
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(paths.css, ['sass']);
});

/**
 * Default build task
 */
gulp.task('default', ['sass']);
