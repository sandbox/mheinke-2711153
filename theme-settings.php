<?php
/**
 * Implements hook_form_system_theme_settings_alter.
 * @param $form
 * @param $form_state
 */
function hathway_theme_kit_form_system_theme_settings_alter(&$form, $form_state) {
    $form['fixed_header'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Make Header Area Fixed'),
        '#default_value' => theme_get_setting('fixed_header'),
        '#description'   => t("Check this box to make the header fixed to the top of the page"),
    );
}